# PiperCI Installer

[![Build Status](https://gitlab.com/dreamer-labs/piperci/piperci-installer/badges/master/pipeline.svg)](https://gitlab.com/dreamer-labs/piperci/piperci-installer)

## Purpose

This repository contains the code required to setup various environments used to deploy Piper CI for production and CI usage.

## Basic Usage

### Repository Selection

The first step is to decide whether you would like to use the code, as is, in the dreamer-labs/piperci/piperci-installer repo,
or alternately, fork the repo into your own organization. Currently, only www.gitlab.com repositories are supported.

### Environment Provisioning

The suggested way to provision an environment is with Terraform, however you can also deploy a PiperCI installation using only the Ansible role

#### Terraform or "Push" Method

##### Initial Deployment with Terraform to OpenStack cloud

###### Configure Terraform

Install Ansible
`pip3 install ansible==2.7`

Configure your terraform.tfvars file with your key hash. Also ensure your openstackrc file has been sourced. See terraform/terraform.tfvars.example

Install the tf ansible plugin

`bash install-tf-ansible.sh`

`-rwxr-xr-x  1 dreamer_labs  staff  31366360 May  7 14:03 /Users/dreamer_labs/.terraform.d/plugins/terraform-provisioner-ansible_v2.2.0`

`terraform init`

```
Initializing the backend...

Initializing provider plugins...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.null: version = "~> 2.1"
* provider.openstack: version = "~> 1.21"
* provider.template: version = "~> 2.1"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

###### Run Terraform

From `terraform/` run `terraform apply`

##### Deploy faas functions from a local directory

Define your local faas function dir in `roles/ansible-role-pi_install/defaults/main.yml`

```
pi_install_piperci_faas:
  - name: noop_faas
    functions:
      - name: piperci-noop-gateway
        image: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas/gateway:latest"
      - name: piperci-noop-executor
        image: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas/executor:latest"
```

### Environment Resource Access

#### Standard PiperCI install

`ubuntu1804/dockerswarm+openfaas`

This environment is provisioned on an Ubuntu 18.04 LTS server, and includes a regular OpenFaaS installation installed atop docker swarm. This provides the install of PiperCI without all the multi-tenancy, and other bells and whistles provided by the Kubernetes installed OpenFaaS cloud. It is great for performing integration testing of one or more faas functions as part of a CI pipeline.

No special client-side configuration is required to use access this particular environment. Once it has finished deploying, you simply access it here:

`http://${server_ip}:8080/ui/`

The docker swarm networking orchestrator is configured to proxy you to the backend API endpoint for each function, as well as the GUI for OpenFaaS itself. From there, you can use the GUI to manually test/deploy your functions. The current list of installed functions can be found inside the repository your bootstrap script performed the `ansible-pull` command from. It should be located in `/root/.ansible/pull/`. The default repositories are listed in `defaults/ubuntu1804/dockerswarm+openfaas.yml`. See the "Customizing Configuration Script" section for details on how to modify these variables to add in custom functions you are writing. The configuration script is reasonably idempotent, and can be rerun to install new functions without negatively impacting the current running functions.

If you ssh into the server, you will have access to the `faas-cli` while logged into either the root or `pidev` user. This installation of the `faas-cli` can be used to invoke functions to test them. Alternately, you can use the curl-pipe-to-bash script provided by the `faas-cli` maintainers to install it on a different machine. See the `faas-cli` documentation if you would like to take that route, or just use the install provided on your server via the configuration script.

## License

[LICENSE](https://gitlab.com/dreamer-labs/piperci/piperci-picli/blob/master/LICENSE)

## Authors

See the list of [contributors](https://gitlab.com/dreamer-labs/piperci/piperci-installer/-/graphs/master) who participated in this project.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags](https://gitlab.com/dreamer-labs/piperci/piperci-picli/-/tags) on this repository.

## Contributing

Please read [Contributing Guide](https://piperci.dreamer-labs.net/project-info/contributing) for details on our code of conduct, and the process for submitting pull requests.
