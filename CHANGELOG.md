# [1.3.0](https://gitlab.com/dreamer-labs/piperci/piperci-installer/compare/v1.2.3...v1.3.0) (2020-01-22)


### Features

* add security groups ([8cdc8ca](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/8cdc8ca))

## [1.2.3](https://gitlab.com/dreamer-labs/piperci/piperci-installer/compare/v1.2.2...v1.2.3) (2019-12-17)


### Bug Fixes

* Update image id ([0322e7a](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/0322e7a))

## [1.2.2](https://gitlab.com/dreamer-labs/piperci/piperci-installer/compare/v1.2.1...v1.2.2) (2019-11-04)


### Bug Fixes

* Removed invalid parameter to apt ([47b5cf8](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/47b5cf8))

## [1.2.1](https://gitlab.com/dreamer-labs/piperci/piperci-installer/compare/v1.2.0...v1.2.1) (2019-10-08)


### Bug Fixes

* Fix faas override logic ([6a6baf8](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/6a6baf8))

# [1.2.0](https://gitlab.com/dreamer-labs/piperci/piperci-installer/compare/v1.1.0...v1.2.0) (2019-10-02)


### Features

* add faas_templates overrides ([0b65d15](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/0b65d15))

# [1.1.0](https://gitlab.com/dreamer-labs/piperci/piperci-installer/compare/v1.0.1...v1.1.0) (2019-10-02)


### Bug Fixes

* add debugging of overrides ([e4a37e1](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/e4a37e1))
* add install tag ([7db8b32](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/7db8b32))
* allow host to be localhost ([8f24617](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/8f24617))
* apply variables to dev role ([8ab677e](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/8ab677e))
* apply vars to openfaas task ([95db22a](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/95db22a))


### Features

* add local builds for faas functions ([96411e1](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/96411e1))

## [1.0.1](https://gitlab.com/dreamer-labs/piperci/piperci-installer/compare/v1.0.0...v1.0.1) (2019-09-30)


### Bug Fixes

* Apply overrides based on env vars ([0795bfc](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/0795bfc))

# 1.0.0 (2019-09-16)


### Features

* add dev env installations ([c0f8bfb](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/c0f8bfb))
