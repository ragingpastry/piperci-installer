Pi Install
=========

An Ansible role to automate the installation of PiperCI infrastructure.

Requirements
------------

None


Role Variables
--------------

* pi_install_packages  
  Contains a list of package names and versions to be installed.
  * type: list of dictionaries
  * Required: True
  * Example:
    ```
	pi_install_packages:
	  - name: "docker-ci"
	    version: "5:19.03.1~3-0~ubuntu-bionic"
    ```

* pi_install_python_packages  
  Contains a list of Python package names and versions to be installed.
  * type: list of dictionaries
  * required: True
  * example:
    ```
	pi_install_python_packages:
	  - name: "docker"
	    version: "4.0.2"
	```

* pi_install_docker_compose  
  Contains the URI of the docker-compose binary
  * type: dict
  * required: True
  * example:
    ```
	pi_install_docker_compose:
	  uri: "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-Linux-x86_64"
	```

* pi_install_piperci_gman  
  Contains the location of the PiperCI Gman docker image
  * type: dict
  * required: True
  * example:
    ```
	pi_install_piperci_gman:
	  image: "registry.gitlab.com/dreamer-labs/piperci/piperci-gman:latest
	```

* pi_install_openfaas  
  Contains a Git repository location and version for the installation of OpenFaaS
  * type: dict
  * required: True
  * example:
    ```
	pi_install_openfaas:
	  repo: "https://github.com/openfaas/faas"
	  version: "master"
	```

* pi_install_faas_cli  
  Contains a location and version for the FaaS Cli binary
  * type: dict
  * required: True
  * example:
    ```
	pi_install_faas_cli:
	  uri: "https://github.com/openfaas/faas-cli/releases/download/0.9.0/faas-cli"
      version: "0.9.0"
	```

* pi_install_piperci_faas  
  Contains a list of dictionaries which define the PiperCI FaaS images to install in the environment. 
  * type: dict
  * required: True
  * example:
    ```
	pi_install_piperci_faas:
	  - name: noop_faas
        functions:
          - name: piperci-noop-gateway
            image: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas/gateway:latest"
          - name: piperci-noop-executor
            image: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas/executor:latest"
    ```

* pi_install_piperci_faas_template  
  Contains a URI and version for a git repository containing the FaaS templates which are installed using the `faas template pull` command.
  * type: dict
  * required: True
  * example:
    ```
    pi_install_piperci_faas_template:
      uri: "https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates.git"
      version: "master"
    ```

Dependencies
------------

An optional dependency for this role is the `ansible-release_piperci_releases` role, which will read a PiperCI releases file and define the required variables for this role.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: servers
      roles:
         - role: read-piperci-releases
           read_releases_url: "https://gitlab.com/dreamer-labs/piperci/piperci-releases.git"
           read_releases_version: "master"
         - role: ansible-role-pi_install
           pi_install_packages: "{{ piperci_installer.packages[ansible_os_family] }}"
           pi_install_python_packages: "{{ piperci_installer.packages.python }}"
           pi_install_docker_compose:
             uri: "{{ piperci_installer.docker_compose.uri }}"
           pi_install_piperci_gman:
             image: "{{ piperci_installer.gman.image }}:{{ piperci_installer.gman.tag }}"
           pi_install_openfaas:
             repo: "{{ piperci_installer.openfaas.uri }}"
             version: "{{ piperci_installer.openfaas.version }}"
           pi_install_faas_cli: "{{ piperci_installer.faas_cli }}"
           pi_install_piperci_faas: "{{ piperci_installer.faas }}"
           pi_install_piperci_faas_template: "{{ piperci_installer.faas_templates }}"
```           

License
-------

MIT

Author Information
------------------

Written by DreamerLabs
